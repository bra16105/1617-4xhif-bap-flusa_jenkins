
import com.sun.org.apache.xpath.internal.operations.NotEquals;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sabi on 23.10.2016.
 */
public class PersonTest {
    @Test
    public void MakeAPersonNamendFranzHans() throws Exception {
        Person p = new Person("Franz", "Hans");

        assertThat(p.getFirstname(), equalTo("Franz"));
        assertThat(p.getLastname(), equalTo("Hans"));
    }

    @Test
    public void MakeAPersonNamendKarlHeinzWhosADarsteller() throws Exception {
        Person p = new Person("Karl", "Heinz", "Darsteller");

        assertThat(p.getFirstname(), equalTo("Karl"));
        assertThat(p.getLastname(), equalTo("Heinz"));
        assertThat(p.getType(), equalTo("Darsteller"));
    }

    @Test
    public void MakeAPersonNamendFranzHansWhosARegisseur() throws Exception{
        Person p = new Person("Franz", "Hans", "Regisseur");

        assertThat(p.getFirstname(), equalTo("Franz"));
        assertThat(p.getLastname(), equalTo("Hans"));
        assertThat(p.getType(), equalTo("Regisseur"));
    }

    @Test(expected=Exception.class)
    public void MakeAPersonWithoutAnyName() throws Exception{
        Person p = new Person("", null);
    }

    @Test(expected=Exception.class)
    public void MakeAPersonWithAnUndefiniedType() throws Exception{
        Person p = new Person("Karl", "Heiz", "Caterer");
    }

    @Test
    public void MakeTwoPersonsNamendFranzHansWhichAreRegisseurs() throws Exception{
        Person p = new Person("Franz", "Hans", "Regisseur");
        Person p2 = new Person("Franz", "Hans", "Regisseur");

        assertNotEquals(p, p2);
    }
}
