
import java.util.ArrayList;

/**
 * Created by Sabi on 06.10.2016.
 */
public class Person {

    private String firstname;
    private String lastname;
    private String type;
    private ArrayList<String> typeList;

    /**
     * Minimum constructor
     * @param firstname of the person
     * @param lastname of the person
     * @throws Exception if first- or lastname is null or empty
     */
    public Person (String firstname, String lastname) throws Exception {
        typeList = new ArrayList<String>();
        typeList.add("Darsteller");
        typeList.add("Regisseur");

        this.setFirstname(firstname);
        this.setLastname(lastname);
    }

    /**
     * Constructor
     * @param firstname of the person
     * @param lastname of the person
     * @param type of the person
     * @throws Exception if first- or lastname or type is null or empty
     */
    public Person (String firstname, String lastname, String type) throws Exception {
        typeList = new ArrayList<String>();
        typeList.add("Darsteller");
        typeList.add("Regisseur");

        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setType(type);
    }

    /**
     * Sets Firstname
     * @param firstname of the person
     * @throws Exception if firstname is null or empty
     */
    public void setFirstname (String firstname) throws Exception {
        if(firstname != null && !firstname.equals("")) {
            this.firstname = firstname;
        }
        else {
            throw new Exception("Es muss ein Vorname angegeben werden.");
        }
    }

    /**
     * Returns firstname
     * @return firstname of the person
     */
    public String getFirstname(){
        return firstname;
    }

    /**
     * Sets lastname
     * @param lastname of the person
     * @throws Exception if lastname is null or empty
     */
    public void setLastname (String lastname) throws Exception {
        if(lastname != null && !lastname.equals("")) {
            this.lastname = lastname;
        }
        else {
            throw new Exception("Es muss ein Nachname angegeben werden.");
        }
    }

    /**
     * Returns lastname
     * @return lastname of the person
     */
    public String getLastname(){
        return lastname;
    }

    /**
     * Sets type
     * @param type of the person
     * @throws Exception if the type is null or empty or if the type is not in the typeList
     */
    public void setType(String type) throws Exception {
        if(type != null && !type.equals("")) {
            if(typeList.contains(type)) {
                this.type=type;
            }
            else {
                throw new Exception("Der angegebene Typ ist ungültig.");
            }
        }
    }

    /**
     * Returns type
     * @return type of the person
     */
    public String getType(){
        return type;
    }
}
