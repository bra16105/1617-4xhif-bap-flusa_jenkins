package bowling;

import org.junit.Test;

import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;


public class GameTest {

    @Test
    public void rollTwentyTimesWithZeroPins() {
        Game game = new Game();

        IntStream.range(1,21).forEach(i -> game.roll(0));

        assertThat(game.score(), equalTo(0));
    }


    @Test
    public void rollOnesWithSixPinsAndOnesWithThree() {
        Game game = new Game();

        game.roll(6);
        game.roll(3);

        assertThat(game.score(), equalTo(9));
    }

    @Test
    public void rollMakeASpareAndRollOnceWithThreePinsAndOnceWithTwo() {
        Game game = new Game();

        game.roll(6);
        game.roll(4);
        game.roll(3);
        game.roll(2);

        assertThat(game.score(), equalTo(18));
    }

    @Test
    public void rollMakeTwoSparesAndRollOnceWithThreePinsAndOnceWithTwo() {
        Game game = new Game();

        game.roll(6);
        game.roll(4);
        game.roll(7);
        game.roll(3);
        game.roll(3);
        game.roll(2);

        assertThat(game.score(), equalTo(35));
    }

    @Test
    public void rollMakeThreeSparesAndRollOnceWithThreePinsAndOnceWithTwo() {
        Game game = new Game();

        game.roll(6);
        game.roll(4);
        game.roll(7);
        game.roll(3);
        game.roll(8);
        game.roll(2);
        game.roll(3);
        game.roll(2);

        assertThat(game.score(), equalTo(53));
    }

    @Test
    public void rollMakeAStrike() {
        Game game = new Game();

        game.roll(10);

        assertThat(game.score(), equalTo(10));
    }

    @Test
    public void rollMakeAStrikeAndRollOnceWithThreePinsAndOnceWithTwo() {
        Game game = new Game();

        game.roll(10);
        game.roll(3);
        game.roll(2);

        assertThat(game.score(), equalTo(20));
    }

    @Test
    public void rollMakeTwoStrikesAndRollOnceWithThreePinsAndOnceWithFour() {
        Game game = new Game();

        game.roll(10);
        game.roll(10);
        game.roll(3);
        game.roll(4);

        assertThat(game.score(), equalTo(47));
    }

    @Test
    public void rollMakeThreeStrikesAndRollOnceWithThreePinsAndOnceWithFour() {
        Game game = new Game();

        game.roll(10);
        game.roll(10);
        game.roll(10);
        game.roll(3);
        game.roll(4);

        assertThat(game.score(), equalTo(77));
    }

    @Test
    public void rollMakeFourStrikes() {
        Game game = new Game();

        game.roll(10);
        game.roll(10);
        game.roll(10);
        game.roll(10);

        assertThat(game.score(), equalTo(90));
    }

    @Test
    public void perfectGame() {
        Game game = new Game();

        IntStream.range(1,12).forEach(i -> game.roll(10));

        assertThat(game.score(), equalTo(300));
    }
}
