package bowling;

import java.util.ArrayList;

public class Game {

    private int score;

    private ArrayList<ArrayList<Integer>> game = new ArrayList<ArrayList<Integer>>();
    private ArrayList<Integer> frame = new ArrayList<Integer>();

    public void roll(int pins) {

        if (frame.size() == 2) {

            if (frame.get(0) != 10 && frame.get(1) != 10) {
                if (frame.get(0) + frame.get(1) == 10)
                    score += pins;
            }
            game.add(frame);
            frame = new ArrayList<Integer>();
        }

        frame.add(pins);
        score += pins;

        if (pins == 10)
            frame.add(0);

        if (frame.size() == 2) {
            if (game.size() != 0) {
                ArrayList<Integer> last = game.get(game.size() - 1);
                if (last.get(0) == 10 || last.get(1) == 10) {
                    score += frame.get(1);
                    score += frame.get(0);
                }

                if (game.size() > 1) {
                    ArrayList<Integer> secondToLast = game.get(game.size() - 2);
                    if (secondToLast.get(0) == 10 || secondToLast.get(1) == 10) {
                        score += frame.get(0);
                    }
                }
            }
        }

    }

    public int score() {
        return score;
    }
}

